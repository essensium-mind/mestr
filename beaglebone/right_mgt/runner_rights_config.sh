#!/bin/bash

# This script configures the rights of the gitlab-runner user,
# and the file capabilities of the tcpdump and ip binaries.
# 
# It is needed to be able to run network related tests, or tests
# that use GPIO or the serial interface without beeing root.
# 
# It assumes that the gitlab-runner has already been installed
# (or that the gitlab-runner user exists).
# After running this script, all the members of the net-admin-grp
# group (by default, network) will be able to use tcpdump
# and ip without being root.
# Usage :
#          ./runner_rights_config.sh [-n runner-username] [-g net-admin-grp]
# 
#          -n runner-username
#                 Name of the user that needs to run the tests.
#                 (default : 'gitlab-runner')
# 
#          -g net-admin-grp
#                 Name of the group to create, whose members will
#                 be able to use ip and tcpdump whithout being root.
#                 (default : 'network')
# 
#          -h
#                 Print this help and exit.
# 
# As you need root priviledges to change a user's group and to set file capabilities,
# this script must be run as root.

set -e # exit script on failure

function usage() {
    local bold
    bold=$(tput bold)
    local normal
    normal=$(tput sgr0)

    printf "This script configures the rights of the gitlab-runner user,\\nand the file capabilities of the tcpdump and ip binaries.\\n\\n"
    printf "It is needed to be able to run network related tests, or tests\\nthat use GPIO or the serial interface without beeing root.\\n\\n"
    printf "It assumes that the gitlab-runner has already been installed\\n(or that the gitlab-runner user exists).\\n"
    printf "After running this script, all the members of the %snet-admin-grp%s\\ngroup (by default, network) will be able to use tcpdump\\n\
and ip without being root.\\n" "$bold" "$normal"
    printf "Usage :\\n"
    printf "\\t %s [-n runner-username] [-g net-admin-grp]\\n\\n" "$0"
    printf "\\t %s-n runner-username%s\\n\\t\\tName of the user that needs to run the tests.\\n\\t\\t(default : 'gitlab-runner')\\n\\n" "$bold" "$normal"
    printf "\\t %s-g net-admin-grp%s\\n\\t\\tName of the group to create, whose members will\\n\\t\\tbe able to use ip\
 and tcpdump whithout being root.\\n\\t\\t(default : 'network')\\n\\n" "$bold" "$normal"
    printf "\\t %s-h%s\\n\\t\\tPrint this help and exit.\\n\\n" "$bold" "$normal"
    printf "As you need root priviledges to change a user's group and to set file capabilities,\\nthis script must be run as root.\\n"
}

function is_root() {
    if [ "$EUID" -ne 0 ]; then
        echo -e "Please run as root"
        exit 1
    fi
}

runner_user="gitlab-runner"
net_admin_grp="network"
args=$(getopt -o "p:a:w:hds:" -- "$@")

eval set -- "$args"

while [ $# -ge 1 ]; do
    case "$1" in
    --)
        # No more options left.
        shift
        break
        ;;
    -n)
        runner_user="$2"
        shift
        ;;
    -g)
        net_admin_grp="$2"
        shift
        ;;
    -h)
        usage
        exit 0
        ;;
    esac

    shift
done

is_root

apt update
apt install -y tcpdump libcap2-bin

# Create a network group
groupadd "$net_admin_grp"

# Add the user to the needed groups
usermod -a -G "i2c,gpio,pwm,spi,dialout,eqep,$net_admin_grp" "$runner_user"

# Set the group of ip and tcpdump to network
chgrp network /usr/sbin/tcpdump
chgrp network /bin/ip

# Restrict the execution right for ip and tcpdump to the members of “network” and to the file owner
chmod 750 /usr/sbin/tcpdump
chmod 750 /bin/ip

# Set the needed capabilities
setcap cap_net_raw,cap_net_admin=ep /usr/sbin/tcpdump
setcap cap_net_admin=ep /bin/ip

