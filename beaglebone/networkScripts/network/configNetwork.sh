#!/bin/bash
set -o nounset
set -o errexit

function listInterface() { # listInterface returnInterfaceList nbInterfaces
    # Makes a list of the available ethernet interface, and count them
    local -n interfaces=$1
    local -n nbInterfaces=$2
    interfaces=""
    first_loop=true
    for found in /sys/class/net/*; do
        [[ -e $found ]] || continue
        # The script can be used to also select wifi interface by replacing the grep expression with "[ew].*"
        found=$(echo "$found" | sed -n -e "s/.*\\/\\(e.*\\)/\\1/p") # keep only the name of the interface
        [[ $found != "" ]] &&
            if $first_loop; then
                interfaces="$found" # First time -> Set
                first_loop=false
            else
                interfaces="$interfaces
$found" # After -> Append
            fi
    done
    nbInterfaces=$(echo -n "$interfaces" | grep -c '^')
}
function askInterfaceIfNeeded() { # askInterfaceIfNeeded interfaceList interfaceToChoose interfaceChosen message
    local list=$1
    local -n interface=$2
    local -n chosenInterface=$3
    local message=$4
    if [ "$interface" = "__UNDEFINED__ " ] || [ "$interactive" == 2 ] || ! checkInterfaceExists "$interface" || [ "$interface" == "$chosenInterface" ]; then
        echo "$message"
        select interface in ${list}; do
            if [ "$interface" != "" ]; then
                break
            fi
            echo "Please choose one of the proposed interfaces."
        done
    fi
}
function selectInterface() { # selectInterface parameters
    # If both interfaces are not defined (or don't exist), or if both interfaces are the same, asks for the missing informations.
    local -n params=$1
    askInterfaceIfNeeded "${params['interfaces']}" params['wanInterface'] params['lanInterface'] "Select the interface to use as WAN interface : "

    local interfacesNoWan=${params['interfaces']//${params['wanInterface']}/} #creates a temporary list wich does not contain the interface chosen as wan interface.
    askInterfaceIfNeeded "$interfacesNoWan" params['lanInterface'] params['wanInterface'] "Select the interface to use as LAN interface : "
}
function askVariableIfNeeded() { # askVariableIfNeeded variable "$message"
    # Prompts the user for a value using the message if the value is undefined, or if the interactivity level is 2.
    local -n variable=$1

    if [ "$variable" == "__UNDEFINED__" ] || [ "$interactive" == 2 ]; then
        read -r -p "$2" variable
    fi
}
function askYesNo() { # askYesNo variable "$message"
    # Prompts the user for yes or no using the message.
    local -n var=$1
    local message=$2
    #     local answer="__UNDEFINED__"
    while true; do
        askVariableIfNeeded var "$2 (yes/no) : "
        case $var in
        [Yy]* | 1)
            var=1
            break
            ;;
        [Nn]* | 0)
            var=0
            break
            ;;
        *)
            echo "Please enter yes or no."
            var="__UNDEFINED__"
            ;;
        esac
    done
}
function getMacWithoutColumn() { # getMacWithoutColumn ifaceName
    local interface=$1
    sed 's/://g' /sys/class/net/"${interface}"/address # Gets the mac address from the interface, and replace the columns with nothing.
}
function setIfaceLAN() { # setIfaceLAN parameters
    local -n params=$1
    local lanInterface=${params['lanInterface']}
    local lanInterfaceHaddr
    lanInterfaceHaddr=$(getMacWithoutColumn "$lanInterface") # Gets the mac address without its column to build the service name.
    local lanServiceName="ethernet_${lanInterfaceHaddr}_cable"

    echo "Setting $lanInterface static IP"
    sudo connmanctl config "$lanServiceName" --ipv4 manual "${params['lanIPAddr']}" "${params['lanNetmask']}" "${params['lanGateway']}"
}
function setIfaceWAN() { # setIfaceWAN $ifaceName
    local wanInterface=$1
    local wanInterfaceHaddr
    wanInterfaceHaddr=$(getMacWithoutColumn "$wanInterface") # Gets the mac address without its column to build the service name.
    local wanServiceName="ethernet_${wanInterfaceHaddr}_cable"

    echo "Setting $wanInterface to use DHCP"
    sudo connmanctl config "$wanServiceName" --ipv4 dhcp
}
function configureDHCPServer() { # configureDHCPServer parameters
    # Fills the placeholders of the dnsmasq config template using the parameters,
    # and replace the previous dnsmasq.conf file by this new one.
    # This function assumes that all the needed parameters are included in the
    # params associative array.
    # The previous dnsmasq.conf is backed up in a dnsmasq.conf.old file.


    local -n params=$1

    echo "Setting up the dhcp serveur on ${params['lanInterface']}, with : "
    printf "\\tDhcp range : %s\\n" "${params['minDhcpIP']}-${params['maxDhcpIP']}"
    printf "\\tMaximum leases : %s\\n" "${params['minDhcpIP']}-${params['maxDhcpIP']}"
    printf "\\tLease time : %s\\n" "${params['leasetime']}"
    printf "\\tDomain : %s\\n" "${params['domain']}"
    printf "\\tisAuthoritative : %s\\n" "${params['isAuthoritative']}"
    printf "\\tDefault gateway activated : %s\\n" "${params['defaultGatewayActivated']}"
    if [ "${params['staticDutIP']}" == 1 ]; then
        printf "\\tDUT mac address: %s\\n" "${params['dutMacAdress']}"
        printf "\\tDUT static IP: %s\\n" "${params['dutIPAdress']}"
    fi

    # Keep only the rightmost number.
    max=${params['maxDhcpIP']##*.}
    min=${params['minDhcpIP']##*.}
    # Compute the number of available IP.
    maxLease=$((max-min))

    # Filling the template config file :
    sed -e "s/{INTERFACE}/${params['lanInterface']}/" "${DIR}/dnsmasq.conf.template" >"${DIR}/dnsmasq.conf.temp"
    sed -i -e "s/{MINDHCPIP}/${params['minDhcpIP']}/" "${DIR}/dnsmasq.conf.temp"
    sed -i -e "s/{MAXDHCPIP}/${params['maxDhcpIP']}/" "${DIR}/dnsmasq.conf.temp"
    sed -i -e "s/{DHCPLEASEMAX}/${maxLease}/" "${DIR}/dnsmasq.conf.temp"
    sed -i -e "s/{LEASETIME}/${params['leasetime']}/" "${DIR}/dnsmasq.conf.temp"
    sed -i -e "s/{DOMAIN}/${params['domain']}/" "${DIR}/dnsmasq.conf.temp"
    sed -i -e "s/{LANIPADDR}/${params['lanIPAddr']}/" "${DIR}/dnsmasq.conf.temp"

    # Uncommenting lines of the config file if needed:
    if [ "${params['isAuthoritative']}" == 1 ]; then
        sed -i '/dhcp-authoritative/s/^#*//' "${DIR}/dnsmasq.conf.temp"
    fi
    if [ "${params['defaultGatewayActivated']}" == 0 ]; then
        sed -i '/dhcp-option=3/s/^#*//' "${DIR}/dnsmasq.conf.temp"
    fi
    if [ "${params['staticDutIP']}" == 1 ]; then
        sed -i '/dhcp-host={DUTMACADR},{DUTIPADR}/s/^#*//' "${DIR}/dnsmasq.conf.temp"
        sed -i -e "s/{DUTMACADR}/${params['dutMacAdress']}/" "${DIR}/dnsmasq.conf.temp"
        sed -i -e "s/{DUTIPADR}/${params['dutIPAdress']}/" "${DIR}/dnsmasq.conf.temp"
    fi

    # Setting the right ownership and rights on the new config file:
    chmod 0644 "${DIR}/dnsmasq.conf.temp"
    sudo chown root:root "${DIR}/dnsmasq.conf.temp"
    # Moving the new config file in the right location
    sudo mv -b -S .old "${DIR}/dnsmasq.conf.temp" "${dnsmasqConfPath}dnsmasq.conf"
    # Checking the configuration before restarting the service :
    if ! /usr/sbin/dnsmasq --test; then
        echo "invalid dnsmasq.conf generated."
        sudo mv "${dnsmasqConfPath}dnsmasq.conf.old" "${dnsmasqConfPath}dnsmasq.conf"
        exit 1
    # If the config is OK, restart the service.
    else
        echo "Restarting dnsmasq."
        sudo systemctl restart dnsmasq.service
    fi
}

function choiceDhcpServer() { # choiceDhcpServer paramList
    # If the server must be configured, configures the server.
    local -n paramList=$1
    if [[ ${paramList['configureDnsmasq']} == 1 ]]; then
        configureDHCPServer paramList
    fi

}
function checkInterfaceExists() { # checkInterfaceExists ifaceName
    # checks if the interface ifaceName exists
    local interface=$1
    [[ "$interface" != "" ]] && ls /sys/class/net/"$interface"/ &>/dev/null
}
function getMode() { # getMode paramList
    # sets the mode to lan or wan according to the provided informations.
    local -n params=$1
    if checkInterfaceExists "${params['lanInterface']}"; then
        params['mode']="lan"
    else
        if checkInterfaceExists "${params['wanInterface']}"; then
            params['mode']="wan"
        else
            params['mode']="__UNDEFINED__"
        fi
    fi
}

function askModeIfNeeded() { # askModeIfNeeded mode
    # Ask for the mode (lan or wan), and proceeds to input verification.
    local -n askedMode="$1"
    while true; do
        askVariableIfNeeded askedMode "Please enter the mode for the interface. (wan/lan) : "
        case "$askedMode" in
        "wan" | "lan")
            break
            ;;
        *)
            echo "Please enter lan or wan."
            askedMode="__UNDEFINED__"
            ;;
        esac
    done

}

function getParameters() { # getParameters parameters
    # Get the parameters from the configuration file, and store them in an associative array.

    local -n obtainedParams=$1

    listInterface obtainedParams['interfaces'] obtainedParams['nbInterfaces']
    if [ "${obtainedParams['nbInterfaces']}" = 0 ]; then
        echo "No ethernet interfaces detected, exiting."
        exit 1
    fi

    # Get the needed informations from the config file.
    obtainedParams+=(['lanInterface']="$(config_get lanInterface)")
    obtainedParams+=(['wanInterface']="$(config_get wanInterface)")

    # Get the settings for the dnsmasq server
    # If there is only one interface on the system, try to deduce the mode (lan or wan) from the configuration.
    if [ "${obtainedParams['nbInterfaces']}" = 1 ]; then
        getMode obtainedParams
    fi
    if [[ "${obtainedParams['mode']}" != 'wan' ]]; then # only makes sense if there is a LAN interface
        obtainedParams+=(['configureDnsmasq']="$(config_get configureDnsmasq)")
        if [ "${obtainedParams['configureDnsmasq']}" = 1 ]; then #If we don't want to configure the server, no need to get the parameters.
            obtainedParams+=(['minDhcpIP']="$(config_get minDhcpIP)")
            obtainedParams+=(['maxDhcpIP']="$(config_get maxDhcpIP)")
            obtainedParams+=(['leasetime']="$(config_get leasetime)")
            obtainedParams+=(['domain']="$(config_get domain)")
            obtainedParams+=(['isAuthoritative']="$(config_get isAuthoritative)")
            obtainedParams+=(['defaultGatewayActivated']="$(config_get defaultGatewayActivated)")
            obtainedParams+=(['lanIPAddr']="$(config_get lanIPAddr)")
            obtainedParams+=(['lanNetmask']="$(config_get lanNetmask)")
            obtainedParams+=(['lanGateway']="$(config_get lanGateway)")
            obtainedParams+=(['staticDutIP']="$(config_get staticDutIP)")
            if [ "${obtainedParams['staticDutIP']}" = 1 ]; then # Get the infos about the dutMacAdress only if needed.
                obtainedParams+=(['dutIPAdress']="$(config_get dutIPAdress)")
                obtainedParams+=(['dutMacAdress']="$(config_get dutMacAdress)")
            fi
        fi
    fi
}

function askParamsIfNeeded() { # askParamsIfNeeded parameters
    # Ask for the missing requested parameters
    local -n paramsAskedAbout=$1

    if [ "${paramsAskedAbout['nbInterfaces']}" = 1 ]; then
            askModeIfNeeded paramsAskedAbout['mode']
            case ${paramsAskedAbout['mode']} in
            "wan")
                paramsAskedAbout['wanInterface']="${paramsAskedAbout['interfaces']}"
                ;;
            "lan")
                paramsAskedAbout['lanInterface']="${paramsAskedAbout['interfaces']}"
                ;;
            *)
                ;;
            esac
    fi
    if (( paramsAskedAbout['nbInterfaces'] > 1 )) || [ "${paramsAskedAbout['mode']}" = "lan" ]; then

        askVariableIfNeeded paramsAskedAbout['lanIPAddr'] "Please enter the IP address of the Lan interface : "
        askVariableIfNeeded paramsAskedAbout['lanNetmask'] "Please enter the netmask for the Lan interface: "
        askVariableIfNeeded paramsAskedAbout['lanGateway'] "Please enter the IP address of the gateway for the Lan interface : "

        askYesNo paramsAskedAbout['configureDnsmasq'] "Configure the dnsmasq server ?"
        if [ "${paramsAskedAbout['configureDnsmasq']}" = 1 ]; then #If we don't want to configure the server, no need to ask for the parameters.
            askYesNo paramsAskedAbout['isAuthoritative'] "Shall the DHCP server be authoritative ?"
            askYesNo paramsAskedAbout['defaultGatewayActivated'] "Shall the default gateway option be sent to the DHCP clients ?"
            askYesNo paramsAskedAbout['staticDutIP'] "Do you want to set a static IP address for the DUT?"
            askVariableIfNeeded paramsAskedAbout['minDhcpIP'] "Please enter the minimum IP address of the DHCP range : "
            askVariableIfNeeded paramsAskedAbout['maxDhcpIP'] "Please enter the maximum IP address of the DHCP range : "
            askVariableIfNeeded paramsAskedAbout['leasetime'] "Please enter the leasetime of the DHCP leases (in the same form as 12h) : "
            askVariableIfNeeded paramsAskedAbout['domain'] "Please enter the domain name : "
            if [ "${paramsAskedAbout['staticDutIP']}" = 1 ]; then
                askVariableIfNeeded paramsAskedAbout['dutIPAdress'] "Please enter the DUT IP address: "
                askVariableIfNeeded paramsAskedAbout['dutMacAdress'] "Please enter the DUT MAC address in the form '01:23:45:67:89:ab' : "
            fi
        fi
        if ((paramsAskedAbout['nbInterfaces'] > 1)); then
            selectInterface paramsAskedAbout
        fi
    fi
}

function checkParameters() { # checkParameters parameters
    # Check wether the parameters are defined or not. Does not check if they are valid.
    local -n paramsToCheck=$1
    if (( paramsToCheck['nbInterfaces'] > 1 )) || [ "${paramsToCheck['mode']}" = "lan" ]; then
        # If there is a LAN interface

        if [[ ${paramsToCheck['configureDnsmasq']} == 1 ]]; then
            # Checking the Dnsmasq parameters
            if [[ ${paramsToCheck['minDhcpIP']} == "__UNDEFINED__"  ||
                    ${paramsToCheck['maxDhcpIP']} == "__UNDEFINED__" ||
                    ${paramsToCheck['leasetime']} == "__UNDEFINED__" ||
                    ${paramsToCheck['domain']} == "__UNDEFINED__"    ||
                    ${paramsToCheck['isAuthoritative']} == "__UNDEFINED__" ||
                    ${paramsToCheck['defaultGatewayActivated']} == "__UNDEFINED__"
                    ]]; then
                echo "$0 : one or more Dnsmasq parameter(s) undefined, aborting."
                exit 1
            fi
            if [[ ${paramsToCheck['staticDutIP']} == 1 && (
                    ${paramsToCheck['dutIPAdress']} == "__UNDEFINED__" ||
                    ${paramsToCheck['dutMacAdress']} == "__UNDEFINED__") ]]; then
                echo "$0 : a parameter requested for static IP allocation is undefined, aborting."
                exit 1
            fi
        fi

        # Testing the interface static IP parameters
        if [[ ${paramsToCheck['lanIPAddr']} == "__UNDEFINED__" ||
                ${paramsToCheck['lanNetmask']} == "__UNDEFINED__" ||
                ${paramsToCheck['lanGateway']} == "__UNDEFINED__" ]]; then
            echo "$0 : one or more lan interface static IP parameters undefined, aborting."
            exit 1
        fi

        # Checking that the mode is defined
        if [[ ${paramsToCheck['nbInterfaces']} == 1 &&
                ${paramsToCheck['mode']} == "__UNDEFINED__" ]]; then

            echo "$0 : interface mode undefined, aborting."
            exit 1
        fi
    fi

    # Checking that the configured interfaces do exists and are not the same.
    if ((paramsToCheck['nbInterfaces'] > 1)); then
        if ! checkInterfaceExists "${paramsToCheck['lanInterface']}"; then
            echo "No interface found with name ${paramsToCheck['lanInterface']}"
            exit 1
        fi
        if ! checkInterfaceExists "${paramsToCheck['wanInterface']}"; then
            echo "No interface found with name ${paramsToCheck['wanInterface']}"
            exit 1
        fi
        if [ "${paramsToCheck['lanInterface']}" == "${paramsToCheck['wanInterface']}" ]; then
            echo "Wan and lan interfaces can't be the same."
            exit 1
        fi
    fi
}


function setAndCheckParameters() { # setAndCheckParameters parameters
    # Gets the parameters from the config files, populates the parameter
    # array with them.
    # Checks if the parameters are defined. If they are not, it either asks
    # for them, or stops the script depending on the interactivity level.

    local -n paramlist=$1

    getParameters paramlist

    # Get the missing parameters only if the interactivity level is high enough.
    if ((interactive >= 1)); then
        askParamsIfNeeded paramlist
    fi

    # At this point, no needed parameter should be undefined
    checkParameters paramlist

}
########## Utilitary function ##########
# Those two fonctions are used to read the config file :
config_read_file() {
    # Get the line of the config file containing the parameter, keep only the value (on the righ oh the "="),
    # and remove any space placed between the "=" and the value.
    ( (grep -E "^[[:space:]]*$2[[:space:]]*=" "$1" 2>/dev/null || echo "VAR=__UNDEFINED__") | cut -d '=' -f 2- |
        sed -r "s/^[[:space:]]//g" |
        sed -r "s/[[:space:]]*(#.*)?[[:space:]]*$//g" )
}
config_get() {
    val="$(config_read_file "$configPath" "${1}")"
    if [ "${val}" = "__UNDEFINED__" ]; then
        val="$(config_read_file "$defaultConfigPath" "${1}")"
    fi
    if [ "${val}" = "" ]; then # we consider empty values as undefined.
        val="__UNDEFINED__"
    fi
    printf -- "%s" "${val}"
}
# This function displays a bit of help :
function usage() {
    local bold
    bold=$(tput bold)
    local normal
    normal=$(tput sgr0)
    printf "This script configures the interface(s) to use them as a lan or wan interface.\\n"
    printf "Usage :\\n"
    printf "\\t%s [-m mode][-c pathToConfig]\\n\\n" "$0"
    printf "\\t%s-m interactivityLevel, --mode=interactivityLevel%s\\n" "${bold}" "${normal}"
    printf "\\t\\tSets the interactivity level of the script: %s0%s: fully automatic, %s1%s: half-automatic, %s2%s: fully interactive. (default : 1)\\n" "${bold}" "${normal}" "${bold}" "${normal}" "${bold}" "${normal}"
    printf "\\t\\tIn %sfully automatic%s mode, a config file MUST be used, at least to precise the interfaces to be used.\\n" "${bold}" "${normal}"
    printf "\\t\\tIn %shalf-automatic%s mode, if no config file is provided, the script will only ask for the interface to use.\\n\\t\\tYou can make it ask for others parameters by specifying a config file containing these parameters, with empty value.\\n\\t\\tThey will be asked each time you run the script with this config file..\\n" "${bold}" "${normal}"
    printf "\\t\\tIn %sfully interactive%s mode, the script will ask for all the parameters used during the configuration.\\n" "${bold}" "${normal}"

    printf "\\n\\t%s-c pathToConfig, --config=pathToConfig%s\\n" "${bold}" "${normal}"
    printf "\\t\\tDefines the path to a custom config file. If no path is specified, the custom config file will be seeked in the same location as the script\\n"
}
######################### End of functions definition #########################

# variables default values :
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)" # get script directory
defaultConfigPath="${DIR}/config.cfg.defaults"
configPath="${DIR}/config.cfg"
dnsmasqConfPath="/etc/"
interactive=1

if ! args=$(getopt -o "m:c:h" -l mode:,config: -- "$@"); then
    echo "Incorrect options provided"
    exit 1
fi

eval set -- "$args"

while [ $# -ge 1 ]; do
    case "$1" in
    --)
        # No more options left.
        shift
        break
        ;;
    -m | --mode)
        interactive="$2"
        shift
        ;;
    -c | --config)
        configPath="$2"
        if [ ! -s "$configPath" ]; then
            echo "config file doesn't exist or is empty"
            exit 1
        fi
        shift
        ;;
    -h)
        usage
        exit 0
        ;;
    esac

    shift
done

if [ ! -f "$configPath" ] && (( interactive == 0 )); then
    echo "Configuration file not found"
fi

declare -A parameters

setAndCheckParameters parameters

if [ "${parameters['nbInterfaces']}" = 1 ]; then
    case ${parameters['mode']} in
    "wan")
        echo "Setting interface as Wan interface"
        setIfaceWAN "${parameters['wanInterface']}"
        ;;
    "lan")
        echo "Setting interface as Lan interface"
        setIfaceLAN parameters
        choiceDhcpServer parameters
        ;;
    *)
        echo "Lan or Wan mode unspecified"
        exit 1
        ;;
    esac
else
    setIfaceWAN "${parameters['wanInterface']}"
    setIfaceLAN parameters
    choiceDhcpServer parameters
fi
