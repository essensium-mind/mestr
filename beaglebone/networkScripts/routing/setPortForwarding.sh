#!/bin/bash

function usage() {
    local bold
    bold=$(tput bold)
    local normal
    normal=$(tput sgr0)

    printf "This script enables or disables port forwarding between 2 interfaces.\\n"
    printf "It assumes that IP forwarding is already configured.\\n"
    printf "Usage :\\n"
    printf "\\t %s -s state -w wanIF -a localAdress -p port [-h] [-d]\\n\\n" "$0"
    printf "\\t %s-a localAdress%s\\n\\t\\tThe IP address of the DUT\\n\\n" "$bold" "$normal"
    printf "\\t %s-i%s\\n\\t\\tDisplay port redirection informations and exit\\n\\n" "$bold" "$normal"
    printf "\\t %s-h%s\\n\\t\\tPrint this help and exit.\\n\\n" "$bold" "$normal"
    printf "\\t %s-p port%s\\n\\t\\tThe target port to redirect.\\n\\n" "$bold" "$normal"
    printf "\\t %s-d port%s\\n\\t\\tThe wan port to redirect.\\n\\n" "$bold" "$normal"
    printf "\\t %s-s state%s\\n\\t\\tThe state of the port forwarding. \
%sstate%s must be \"enabled\" or \"disabled\".\\n\\n" "$bold" "$normal" "$bold" "$normal"
    printf "\\t %s-w wanIF%s\\n\\t\\tThe name of the wan interface\\n\\n" "$bold" "$normal"
    printf "As you need root priviledges to change routing table configurations, this script must be run as root.\\n"
}

function is_root() {
    if [ "$EUID" -ne 0 ]; then
        echo -e "Please run as root"
        exit 1
    fi
}

function infos() {
    is_root
    iptables -t nat -L PREROUTING -v -n
}

state="enabled"
localAdress="192.168.0.131"
localPort="80"
wanPort=""
wanIF="eth0"
args=$(getopt -o "p:a:w:hid:s:" -- "$@")

eval set -- "$args"

while [ $# -ge 1 ]; do
    case "$1" in
    --)
        # No more options left.
        shift
        break
        ;;
    -s)
        state="$2"
        shift
        ;;
    -p)
        localPort="$2"
        shift
        ;;
    -d)
        wanPort="$2"
        shift
        ;;
    -a)
        localAdress="$2"
        shift
        ;;
    -w)
        wanIF="$2"
        shift
        ;;
    -h)
        usage
        exit 0
        ;;
    -i)
        infos
        exit 0
        ;;
    esac

    shift
done

if [ -z "$wanPort" ]; then
    wanPort="$localPort" # If no wan port is given, target port and wan port are the same.
fi

if { [ "$state" != "enabled" ] && [ "$state" != "disabled" ]; } || [ -z "$wanPort" ] || [ -z "$localAdress" ] || [ -z "$wanIF" ] || [ -z "$localPort" ]; then
    echo "The port, local device address and interface name must be specified."
    usage
else
    is_root
    if [ "$state" == "enabled" ]; then
        iptables -t nat -A PREROUTING -p tcp --dport "$wanPort" -i "$wanIF" -j DNAT --to "$localAdress":"$localPort"
    else
        iptables -t nat -D PREROUTING -p tcp --dport "$wanPort" -i "$wanIF" -j DNAT --to "$localAdress":"$localPort"
    fi
fi
