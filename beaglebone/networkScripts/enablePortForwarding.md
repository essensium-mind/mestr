# Port forwarding

To access http resources on the device under test, through the IP address of the test platform, port forwarding must be enabled.
This is achieved through nat, using iptables.

```
iptables -t nat -A PREROUTING -p tcp --dport <platform port> -i <platform wan if> -j DNAT --to <DUT address>:<DUT port>
```

Reference : [Nat HOWTO](https://netfilter.org/documentation/HOWTO/NAT-HOWTO.html) (a bit old, but it works)
