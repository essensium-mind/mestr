@manager.network @manager.ssh
Feature: testing the ssh server

@normal
Scenario: ssh connexion
   Given 'network' has to be 'up'
    And 'ssh' has to be 'logged out'
    When I set 'ssh' to 'logged in' state
    Then 'ssh' should be 'logged in'
    When I set 'ssh' to 'logged out' state
    Then 'ssh' should be 'logged out'
