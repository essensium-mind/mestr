@integrity
Feature: testing hardware integrity

    @manager.power
    @manager.debug
    Scenario: boot integrity
        Given 'power' has to be 'off'
        When I set 'power' 'on'
        Then 'debug' should receive 'Dobiss system' in the '120' following seconds
        
        
    @fixture.must_boot
    @manager.network
    @manager.power
    Scenario: DHCP integrity
        Given 'power' has to be working
         And 'power' has to be 'off'
        When I set 'power' 'on'
        And 'debug' receives 'Uncompressing Linux'
        And 'debug' receives 'eth0: link becomes ready'
        Then a DHCP operation should happen on 'network' within '15' seconds
        And 'debug' should receive 'Dobiss system' in the '60' following seconds

    
    @manager.api
    @manager.debug
    @manager.power
    Scenario: api integrity
        Given 'network' has to be working
        And Dobiss has to be booted
        Then 'api' is able to reach the web server
