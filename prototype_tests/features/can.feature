@manager.network @manager.ssh @manager.can
Feature: testing the can link

@normal
Scenario: sending packet from the DUT
   Given 'network' is 'up'
     And 'ssh' has to be 'logged in'
    When I send the CAN packet '123#0123456789abcdef' from the DUT using 'ssh'
    Then I should receive '123#0123456789abcdef' on 'can'

@normal
Scenario: sending packet to the DUT
   Given 'network' has to be 'up'
     And 'ssh' has to be 'logged in'
    When I listen for CAN packets on the DUT using 'ssh'
     And 'can' sends '123#0123456789abcdef'
    Then I should receive 'can0  123   \[8\]  01 23 45 67 89 AB CD EF' on the DUT using 'ssh'
