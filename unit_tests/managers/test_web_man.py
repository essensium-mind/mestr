from mestr.managers import web_man
from ..managers import test_manager


class TestWebManager(test_manager._TestManagerApi):

    manager = web_man.WebRequest()
    _man_type = "WebRequest"
    _initial_state = "logged out"

    def test_create_empty_webRequest(self):
        assert self.manager.headers is None
        assert self.manager.timeout is None
        assert self.manager._clean_log_req is None
        assert self.manager._base_scheme == "http"
        assert self.manager._base_netloc is None
        assert self.manager.cookies is None

    def test_update_webRequest_url(self):
        test = web_man.WebRequest(domain="it.bike.be", protocol="http")
        test.update_base_url(domain="my.unicycle.be", protocol="https")
        assert test._base_netloc == "my.unicycle.be"
        assert test._base_scheme == "https"
