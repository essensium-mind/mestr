"""Unit tests for the GPIO manager."""

import unittest.mock
import pytest

from mestr.managers import gpio_man
from unit_tests.managers import test_manager
from unit_tests.stubs import GPIO_stub as GPIO


# pylint: disable=unused-argument
def side_effect_setup(channel, direction, pull_up_down=GPIO.PUD_DOWN,
                      initial=0, delay=0)->None:
    """Side effect function : raises an exception if a specific pin name is
    given, to mimic the behavior of the actual library.
    """
    if channel == "WRONG_PIN":
        msg = "<built-in function setup> returned NULL without setting an error"
        raise SystemError(msg)

gpio_man.GPIO = unittest.mock.Mock(spec=GPIO)
gpio_man.GPIO.input.return_value = 0
gpio_man.GPIO.event_detected.return_value = False
gpio_man.GPIO.gpio_function.return_value = 0
gpio_man.GPIO.setup.side_effect = side_effect_setup
gpio_man.GPIO.HIGH = 1
gpio_man.GPIO.LOW = 0
gpio_man.GPIO.OUT = 1
gpio_man.GPIO.IN = 0
gpio_man.GPIO.ALT0 = 4
gpio_man.GPIO.PUD_OFF = 0
gpio_man.GPIO.PUD_UP = 2
gpio_man.GPIO.PUD_DOWN = 1
gpio_man.GPIO.RISING = 1
gpio_man.GPIO.FALLING = 2
gpio_man.GPIO.BOTH = 3
gpio_man.GPIO.VERSION = "0.0.20"


class TestGpioManager(test_manager._TestManagerApi):
    """Test class for the GPIO manager."""
    _pin = "P8_10"
    manager = gpio_man.GpioManager(_pin)
    _man_type = "GPIO"
    _initial_state = "low"

    def test_creating_the_object_with_default_value(self)->None:
        """Test if the default attribute are correct"""
        gpio_man.GPIO.setup.reset_mock()
        gpio_man.GpioManager(self._pin)
        gpio_man.GPIO.setup.assert_called_with(self._pin, direction=GPIO.IN,
                                               pull_up_down=GPIO.PUD_OFF,
                                               initial=0)

    def test_creating_the_object_with_non_default_value(self)->None:
        """Test if the non default attribute are passed correctly"""
        gpio_man.GPIO.setup.reset_mock()
        gpio_man.GpioManager(self._pin, direction=GPIO.OUT, value=1)
        gpio_man.GPIO.setup.assert_called_with(self._pin, direction=GPIO.OUT,
                                               pull_up_down=GPIO.PUD_OFF,
                                               initial=1)

    @pytest.mark.parametrize(["value", "mode", "physical_value"],
                             [
                                 (0, "ACT_HIGH", 0),
                                 (1, "ACT_HIGH", 1),
                                 (0, "ACT_LOW", 1),
                                 (1, "ACT_LOW", 0)
                             ])
    def test_creating_object_with_non_default_value(self, value, mode,
                                                    physical_value)->None:
        """Test if the non default attribute are passed correctly"""
        gpio_man.GPIO.setup.reset_mock()
        gpio_man.GpioManager(self._pin, direction=GPIO.OUT,
                             mode=mode, value=value)
        gpio_man.GPIO.setup.assert_called_with(self._pin, direction=GPIO.OUT,
                                               pull_up_down=GPIO.PUD_OFF,
                                               initial=physical_value)

    def test_creating_the_object_with_text_value_out(self)->None:
        """Test if the object behaves correctly when given a text argument
        instead of a constant, in output mode
        """
        gpio_man.GPIO.setup.reset_mock()
        gpio_man.GpioManager(self._pin, direction="OUT", value=1)
        gpio_man.GPIO.setup.assert_called_with(self._pin, direction=GPIO.OUT,
                                               pull_up_down=GPIO.PUD_OFF,
                                               initial=1)

    def test_creating_the_object_with_text_value_in(self)->None:
        """Test if the object behaves correctly when given a text argument
        instead of a constant, in input mode
        """
        gpio_man.GPIO.setup.reset_mock()
        gpio_man.GpioManager(self._pin, direction="IN", pull_up_down="PUD_UP",
                             value=1)
        gpio_man.GPIO.setup.assert_called_with(self._pin, direction=GPIO.IN,
                                               pull_up_down=GPIO.PUD_UP,
                                               initial=0)

    def test_creating_the_object_with_bad_pin(self)->None:
        """Test if the correct exception is raised when an error occurs
        on pin setup.
        """
        with pytest.raises(ValueError):
            gpio_man.GpioManager("WRONG_PIN")

    @pytest.mark.parametrize(["pin_value", "mode", "expected_state",
                              "unexpected_state"],
                             [
                                 (0, "ACT_HIGH", "low", "high"),
                                 (1, "ACT_HIGH", "high", "low"),
                                 (0, "ACT_LOW", "high", "low"),
                                 (1, "ACT_LOW", "low", "high")
                             ])
    def test_is_state_standard_input(self, pin_value, mode, expected_state,
                                     unexpected_state)->None:
        """Test the is_state() method, with combination of all the legal values.
        """
        with unittest.mock.patch(
                "mestr.managers.gpio_man.GpioManager.get_value") as m_get_val:
            m_get_val.return_value = pin_value
            man = gpio_man.GpioManager(self._pin, mode=mode)
            assert man.is_state(expected_state)
            assert not man.is_state(unexpected_state)

    @pytest.mark.parametrize(["pin_value", "mode"],
                             [
                                 (0, "ACT_HIGH"),
                                 (1, "ACT_HIGH"),
                                 (0, "ACT_LOW"),
                                 (1, "ACT_LOW")
                             ])
    def test_is_state_inexisting_state(self, pin_value, mode)->None:
        """Test the is_state() method, with combination of all the legal values,
        when asking for a non existent state.
        """
        with unittest.mock.patch(
                "mestr.managers.gpio_man.GpioManager.get_value") as m_get_val:
            m_get_val.return_value = pin_value
            man = gpio_man.GpioManager(self._pin, mode=mode)
            with pytest.raises(ValueError):
                man.is_state("Chocolate")

    @pytest.mark.parametrize(["value", "mode", "physical_value"],
                             [
                                 (0, "ACT_HIGH", 0),
                                 (1, "ACT_HIGH", 1),
                                 (0, "ACT_LOW", 1),
                                 (1, "ACT_LOW", 0)
                             ])
    def test_set_value(self, value, mode, physical_value)->None:
        """Test the set_value() method, with combination of all the legal values,
        in output mode.
        """
        man = gpio_man.GpioManager(self._pin, mode=mode, direction=GPIO.OUT)
        man.set_value(value)
        gpio_man.GPIO.output.assert_called_with(self._pin, physical_value)

    def test_set_value_in_input_mode(self)->None:
        """Test that an exception is raised if the pin is in input mode when
        using set_value().
        """
        man = gpio_man.GpioManager(self._pin, direction=GPIO.IN)
        with pytest.raises(RuntimeError):
            man.set_value(0)

    @pytest.mark.parametrize(["state", "mode", "physical_value"],
                             [
                                 ("low", "ACT_HIGH", 0),
                                 ("high", "ACT_HIGH", 1),
                                 ("low", "ACT_LOW", 1),
                                 ("high", "ACT_LOW", 0)
                             ])
    def test_set_state(self, state, mode, physical_value)->None:
        """Test the set_state() method, with combination of all the legal values,
        in output mode.
        """
        man = gpio_man.GpioManager(self._pin, mode=mode, direction=GPIO.OUT)
        man.set_state(state)
        gpio_man.GPIO.output.assert_called_with(self._pin, physical_value)

    @pytest.mark.parametrize(["state"],
                             [
                                 ("low",),
                                 ("high",)
                             ])
    def test_set_state_in_input_mode(self, state)->None:
        """Test that an exception is raised if the pin is in input mode when
        using set_state().
        """
        man = gpio_man.GpioManager(self._pin, direction=GPIO.IN)
        with pytest.raises(RuntimeError):
            man.set_state(state)

    def test_set_state_with_unknown_state(self)->None:
        """Test that an exception is raised if the state to be set is unknown.
        """
        man = gpio_man.GpioManager(self._pin, direction=GPIO.OUT)
        with pytest.raises(ValueError):
            man.set_state("chocolate")
