"""GPIO functionality of a BeagleBone using Python"""
from typing import Callable, Optional
__name__ = "Adafruit_BBIO.GPIO"
__package__ = "Adafruit_BBIO"

# Constants:
HIGH = 1
LOW = 0
OUT = 1
IN = 0
ALT0 = 4
PUD_OFF = 0
PUD_UP = 2
PUD_DOWN = 1
RISING = 1
FALLING = 2
BOTH = 3
VERSION = "0.0.20"


# setup = <built-in function setup>
def setup(channel: str, direction: int, pull_up_down: int = PUD_DOWN,
          initial: int = 0, delay: int = 0)->None:
    pass


# cleanup = <built-in function cleanup>
def cleanup()->None:
    pass


# output = <built-in function output>
def output(channel: str, value: int)->None:
    pass


# input = <built-in function input>
def input(channel: str)->int:
    pass


# add_event_detect = <built-in function add_event_detect>
def add_event_detect(channel: str, edge: int,
                     callback: Optional[Callable] = None,
                     bouncetime: int = 0)->None:
    pass


# remove_event_detect = <built-in function remove_event_detect>
def remove_event_detect(channel: str)->None:
    pass


# event_detected = <built-in function event_detected>
def event_detected(channel: str)-> bool:
    pass


# add_event_callback = <built-in function add_event_callback>
def add_event_callback(channel: str, callback: Callable, bouncetime: int = 0)->None:
    pass


# wait_for_edge = <built-in function wait_for_edge>
def wait_for_edge(channel: str, edge: int, timeout: int = -1)->None:
    pass


# gpio_function = <built-in function gpio_function>
def gpio_function(channel: str)->int:
    pass


# setwarnings = <built-in function setwarnings>
def setwarnings(gpio_warnings: int)->None:
    pass
