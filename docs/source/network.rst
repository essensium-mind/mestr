
The network configuration scripts
==================================

``configNetwork.sh``
------------------------------
.. _network_conf_ref:

The ``configNetwork.sh`` script does two things: it configures the wan and lan interface to use respectively DHCP and a static IP, and it configures dnsmasq to act as a DNS and DHCP server on the lan interface.

.. warning:: To configure the interfaces, the network manager of the Beaglebone Black needs them to be active. The simplest way to do that safely is to connect a network cable between the two interfaces, and to run the script using the serial console. This way, you are sure to not interfere with the rest of the network (by accidentaly adding a rogue DHCP server for instance), and to not lose your console because of a misconfigured interface.

By default, it will configure a DNS server that only forwards all the requests to the platform's DNS server, and a DHCP server with the following configuration :


========================================= ========================================
            Option                                  Value
========================================= ========================================
Minimum IP address                              192.168.0.50
Maximum IP address                              192.168.0.100
Lease time                                      12h
Domain                                          mestr
The server is authoritative?                    no
The platform is the gateway?                    yes
Does the DUT get a static IP adress?            no
========================================= ========================================


The script works using two configuration files. One of them is the default configuration
file, containing all the default values, and is named ``config.cfg.defaults``. You should
not need to modify it, unless you want to change the default values set by the script.

The other configuration file is by default looked for in the script's directory, under the name
``config.cfg``. It is in this file that you can customize the configuration. An other name or
path can be given using the "-c" parameter in the command line.

The name of the lan and wan interfaces, and the mac address of the DUT are the only options that
do not have a default value, as the two firsts are very specific to the version of the operating
system in use, and the last is very specific to the particular DUT which is being tested.

The possible options and their default values are the following:

.. code-block:: cfg

    lanInterface=                   # Interface name of the LAN interface
    wanInterface=                   # Interface name of the WAN interface
    
    # Configuration for the static IP of the lan interface.
    lanIPAddr=192.168.0.1           # IP adress of the LAN interface
    lanNetmask=255.255.255.0        # Netmask used by the LAN interface
    lanGateway=192.168.0.1          # Gateway used by the LAN interface

    # Configuration for the DHCP server.
    configureDnsmasq=1

    minDhcpIP=192.168.0.50          # Minimum IP address given by the server
    maxDhcpIP=192.168.0.100         # Maximum IP address given by the server
    leasetime=12h                   # Lease time
    domain=mestr                    # Domain
    isAuthoritative=0               # Is the server authoritative? (0: no, 1: yes)
    defaultGatewayActivated=1       # Is the platform the default gateway?
    staticDutIP=0                   # Does the DUT get a static IP adress?
    # Only used if staticDutIP=1
    dutIPAdress=192.168.0.131       # Static IP address of the DUT (chosen arbitrarily,
                                    # not necessarely in the IP pool of the DHCP server)
    dutMacAdress=                   # Mac adress of the DUT




The script has is 3 interactivity levels, that can be chosen using the ""-m" parameter:

0. The script is not interactive at all:
    If a parameter is missing, the script will fail and do nothing.
1. The script is semi-interactive:
    It will use all the values it can find in the configuration or default
    configuration file, and prompt for the missing one. This is the default
    mode.    
2. The script is completely interactive:
    It will prompt for all the parameters, regardless of their presence in
    the configuration files.

An empty parameter prevents the use of the default value :

 - In automatic mode, it causes the script to stop
 - In half-automatic mode, it prompts the user for a value.

For instance:

.. code-block:: cfg

    # This will make the script ask the user wether the dnsmasq server
    # must be configured or not.
    configureDnsmasq=


``setPortForwarding.sh``
------------------------------
.. _port_forw_ref:

To enable port forwarding, you can use the ``setPortForwarding.sh`` script, located in ``beagleBone/networkScript/routing/``.

It will create the correct iptables rule to enable port forwarding. Please keep in mind that IP forwarding must have been enabled for the port forwarding to work.

You can have an overview of the possible options by running:

.. code-block:: bash

    ./setPortForwarding.sh -h

By default (with no other parameters), requests on the port 80 of interface eth0 will be forwarded to port 80 of the device with IP ``192.168.0.131``, which is the default IP we chose for our DUT. This IP address is completely arbitrary.

.. note:: It is not reboot persistent.
