"""Manager managing a serial shell connection"""
import time
from typing import Union, IO, AnyStr, List, Optional
import serial
from pexpect import ExceptionPexpect, TIMEOUT, fdpexpect
from mestr.utils.exceptions import MestrTimeout
from ..utils.auth_helper import get_creds_from_env
from ..utils.types_aliases import Expect
# adafruit library is not compiling out of beaglebone.
# this try statement is added to remove problem at documentation generation
try:
    import Adafruit_BBIO.UART as UART
except ImportError:
    from logging import warning
    warning("project won't work")

from .manager import Manager
from ..utils.timestamped_file import TimestampedFile


def init_pins(channel: str)->None:
    """Device specific pin initialisation.

    This function may be adapted to port MESTR to a platform not
    based on a Beaglebone Black.

    :param channel: Name of the UART to configure, one of UART1, UART2, UART4,
        UART5
    """
    UART.setup(channel)


def levenshtein_distance(word_a: AnyStr, word_b: AnyStr) -> int:
    """This calculates the Levenshtein distance between a and b.
    """

    len_a, len_b = len(word_a), len(word_b)
    if len_a > len_b:
        word_a, word_b = word_b, word_a
        len_a, len_b = len_b, len_a
    current = list(range(len_a+1))
    for i in range(1, len_b+1):
        previous, current = current, [i]+[0]*len_a
        for j in range(1, len_a+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if word_a[j-1] != word_b[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)
    return current[len_a]


# Exception classes used by this manager.
class ExceptionPxserial(ExceptionPexpect):  # type: ignore
    """Raised for SerialManager exceptions.
    """


# pylint: disable=too-many-instance-attributes
class SerialManager(fdpexpect.fdspawn, Manager):  # type: ignore
    """This class extends fdpexpect.fdspawn to specialize setting up serial
    connections. This adds methods for login, logout, and expecting the shell
    prompt.

    The idea is to provide a (nearly) unified interface with pxssh, so that the
    type of connection can be abstracted during automated testing.

    SerialManager uses the shell prompt to synchronize output from the
    remote host.
    In order to make this more robust it sets the shell prompt to something
    more unique than just $ or #. This should work on most Borne/Bash or Csh
    style shells.

    :param auth_creds: base name of env variable where to find credentials. The
            idea is to use credential without pushing it in the git repository
            that we use. The manager will look for ``auth_creds_username`` and
            ``auth_creds_password`` variables to find credentials

        The manager can be defined as :

    .. code-block:: yaml

        -
            type : serial
            name : yourName
            target : "4"
            username : None # optionnal
            password : None # optionnal
            auth_creds : None # optional
            baudrate : 115200 # optionnal
            timeout : 30 # optionnal
            maxread : 2000 # optionnal
            searchwindowsize : None # optionnal

    Example that runs a few commands on a remote server and prints the result::

        import serial_man
        import serial
        import getpass

        ser = serial.Serial('/dev/ttyO4', 115200, timeout=0)
        try:
            s = serial_man.SerialManager(ser, timeout=120, encoding='utf-8')
            username = input('username: ')
            password = getpass.getpass('password: ')
            s.login(username, password)
            s.sendline('uptime')   # run a command
            s.prompt()             # match the prompt
            print(s.before)        # print everything before the prompt.
            s.sendline('ls -l')
            s.prompt()
            print(s.before)
            s.sendline('df')
            s.prompt()
            print(s.before)
            s.logout()
        except serial_man.ExceptionPxserial as e:
            print("SerialManager failed on login.")
            print(e)

    """

    # pylint: disable=too-many-arguments
    def __init__(self,
                 target: int,
                 username: Optional[str] = None,
                 password: Optional[str] = None,
                 baudrate=115200,
                 timeout=30,
                 maxread=2000,
                 searchwindowsize: Optional[int] = None,
                 logfile: IO = None,
                 auth_creds: str = None,
                 encoding='utf-8',
                 codec_errors='replace',
                 **kwargs: Union[int, str]):

        self._available_states = {"logged in": SerialManager.login,
                                  "logged out": SerialManager.logout}
        # Configuring the UART pin :
        self.target = target
        self.baudrate = baudrate
        uart = "UART"+str(target)
        try:
            init_pins(uart)
        except ValueError as error:
            if "Invalid" in str(error):
                raise ValueError(
                    "uart{} does not exist".format(uart)) from error
            else:
                raise ValueError(
                    "pins from uart{} not accessible (used by an overlay) "
                    .format(uart)) from error
        self.serial_object = serial.Serial("/dev/ttyO"+str(target), baudrate)

        # initializing the fdspawn object
        super().__init__(self.serial_object, timeout=timeout,
                         maxread=maxread, searchwindowsize=searchwindowsize,
                         logfile=logfile, encoding=encoding,
                         codec_errors=codec_errors)

        self.name = '<SerialManager>'

        # Storing the login arguments.
        if "login_regex" in kwargs:
            self.login_regex = kwargs["login_regex"]
        else:
            self.login_regex = ""

        if auth_creds:
            self.username, self.password = get_creds_from_env(auth_creds)
        else:
            self.username = username
            self.password = password

        self.opt_login_args = kwargs

        # SUBTLE HACK ALERT! Note that the command that SETS the prompt uses a
        # slightly different string than the regular expression to match it.
        # This is because when you set the prompt the command will echo back,
        # but we don't want to match the echoed command. So if we make the
        # set command slightly different than the regex we eliminate the
        # problem. To make the set command different we add a backslash in
        # front of $. The $ doesn't need to be escaped, but it doesn't hurt and
        # serves to make the set prompt command different than the regex.

        # used to match the command-line prompt
        self.unique_prompt = r"\[PEXPECT\][\$\#] "
        self.curr_prompt = self.unique_prompt

        # used to set shell command-line prompt to unique_prompt.
        self.prompt_set_sh = r"PS1='[PEXPECT]\$ '"
        self.prompt_set_csh = r"set prompt='[PEXPECT]\$ '"

        # to provide compatibility with pxssh
        self.force_password = False

    @staticmethod
    def man_type() -> str:
        """serial"""
        return "serial"

    def try_read_prompt(self, timeout_multiplier: float) -> AnyStr:
        """This facilitates using communication timeouts to perform
        synchronization as quickly as possible, while supporting high latency
        connections with a tunable worst case performance. Fast connections
        should be read almost immediately. Worst case performance for this
        method is timeout_multiplier * 3 seconds.
        """

        # maximum time allowed to read the first response
        first_char_timeout = timeout_multiplier * 0.5

        # maximum time allowed between subsequent characters
        inter_char_timeout = timeout_multiplier * 0.1

        # maximum time for reading the entire prompt
        total_timeout = timeout_multiplier * 3.0

        prompt = self.string_type()
        begin = time.time()
        expired = 0.0
        timeout = first_char_timeout

        while expired < total_timeout:
            try:
                prompt += self.read_nonblocking(size=1, timeout=timeout)
                expired = time.time() - begin   # updated total time expired
                timeout = inter_char_timeout
            except TIMEOUT:
                break

        return prompt

    def sync_original_prompt(self, sync_multiplier: float = 1.0) -> bool:
        """This attempts to find the prompt. Basically, press enter and record
        the response; press enter again and record the response; if the two
        responses are similar then assume we are at the original prompt.
        This can be a slow function. Worst case with the normal sync_multiplier
        can take 12 seconds. Low latency connections are more likely to fail
        with a low sync_multiplier. Best case sync time gets worse with a
        high sync multiplier (500 ms with default). """

        # All of these timing pace values are magic.
        # I came up with these based on what seemed reliable for
        # connecting to a heavily loaded machine I have.
        self.sendline("")
        time.sleep(0.1)

        # Clear the buffer before getting the prompt.
        self.try_read_prompt(sync_multiplier)

        self.sendline("")
        first_att = self.try_read_prompt(sync_multiplier)

        self.sendline("")
        second_att = self.try_read_prompt(sync_multiplier)

        lev_dist = levenshtein_distance(first_att, second_att)
        len_a = len(first_att)
        if len_a == 0:
            return False
        if float(lev_dist)/len_a < 0.4:
            return True
        return False

    # def login(self) -> bool:
        # """This logs the user into the serial shell using the arguments
        # stored in the class.

        # """
        # arguments = {"username": self.username,
        #              "password": self.password, **self.opt_login_args}
        # return self._login(**arguments)

    # pylint: disable=too-many-arguments
    # TODO: This is getting messy and I'm pretty sure this isn't perfect.
    def login(self,
              username: Optional[str] = None,
              password: Optional[str] = None,
              original_prompt=r"[#$]",
              login_timeout=10,
              auto_prompt_reset=True,
              sync_multiplier=1,
              login_regex=r'(?im)^.*login: $',
              password_regex=r'(?i)(?:password:)',
              sync_original_prompt=True)-> bool:
        """This logs the user into the serial shell. It expects the serial
        console to wait on a login prompt.

        It uses
        'original_prompt' to try to find the prompt right after login. When it
        finds the prompt it immediately tries to reset the prompt to something
        more easily matched. The default 'original_prompt' is very optimistic
        and is easily fooled. It's more reliable to try to match the original
        prompt as exactly as possible to prevent false matches by server
        strings such as the "Message Of The Day". On many systems you can
        disable the MOTD on the remote server by creating a zero-length file
        called :file:`~/.hushlogin` on the remote server. If a prompt cannot be
        found then this will not necessarily cause the login to fail. In the
        case of a timeout when looking for the prompt we assume that the
        original prompt was so weird that we could not match it, so we use a
        few tricks to guess when we have reached the prompt. Then we hope
        for the best and blindly try to reset the prompt to something more
        unique. If that fails then login() raises an :class:`ExceptionPxserial`
        exception.

        In some situations it is not possible or desirable to reset the
        original prompt. In this case, pass ``auto_prompt_reset=False`` to
        inhibit setting the prompt to the UNIQUE_PROMPT. Remember that
        SerialManager uses a unique prompt in the :meth:`prompt` method.
        If the original prompt is not reset then this will disable the
        :meth:`prompt` method unless you manually set the :attr:`PROMPT`
        attribute.

        Set ``password_regex`` if there is a MOTD message with `password` in
        it.Changing this is like playing in traffic, don't (p)expect it to
        match straight away.
        """
        username = username or self.username or None
        if username is None:
            raise ValueError("Username is not defined")
        password = password or self.password or None
        if password is None:
            raise ValueError("password is not defined")

        self.login_regex = login_regex
        session_regex_array = [original_prompt, login_regex, password_regex,
                               "(?i)Login incorrect", TIMEOUT]
        if not self.isalive():
            raise ExceptionPxserial('Serial port closed')
        # interrupt potential previous login attempts by sending "ctrl+C"
        self.send('\x03')
        # Empty the buffer to avoid getting two login prompt in a row.
        self.expect("^.*$")
        self.sendline("")
        time.sleep(0.2)
        i = self.expect(session_regex_array, timeout=login_timeout)

        if i != 0:  # Not already logged in
            # First phase
            i = self._login_phase_1(i, username, session_regex_array)

            # Second phase
            i = self._login_phase_2(i, password, session_regex_array)

            # Third phase
            self._login_phase_3(i)

        if sync_original_prompt:
            if not self.sync_original_prompt(sync_multiplier):
                raise ExceptionPxserial(
                    'could not synchronize with original prompt')
        # We appear to be in.
        # set shell prompt to something unique.
        if auto_prompt_reset:
            if not self.set_unique_prompt():
                raise ExceptionPxserial('could not set shell prompt (received:\
                    {0}, expected: {1}).'.format(self.before, self.curr_prompt))
        return True

    def _login_phase_1(self, i: int, username: str,
                       session_regex_array: Expect) -> int:
        """If the login prompt is found, send the username,
        else raise exception
        """
        if i == 1:
            self.sendline(username)
            return self.expect(session_regex_array)
        raise ExceptionPxserial('Unknown state : cannot initiate connection')

    def _login_phase_2(self, i: int, password: str,
                       session_regex_array: Expect) -> int:
        """If the default prompt is found, we suppose we are in,
        else if the password prompt is found, send the password,
        else raise exception
        """
        if i == 0:  # Autologin must be activated
            return 0
        if i == 2:  # Password prompt
            self.sendline(password)
            return self.expect(session_regex_array)

        if i == 1:  # login prompt again
            raise ExceptionPxserial(
                'Error : Got login prompt twice in a row.')
        elif i == 3:  # login incorrect : shouldn't happen
            raise ExceptionPxserial(
                'Unknown state : cannot initiate connection')
        elif i == 4:  # timeout
            raise ExceptionPxserial('login timeout')
        else:
            raise ExceptionPxserial(
                'Unknown state : cannot initiate connection')

    def _login_phase_3(self, i: int) -> None:
        """If the default prompt is found, we suppose we are in,
        else if the login timed out, we hope to be in,
        else we raise an exception
        """
        if i == 0:  # got prompt, probably connected
            pass
        elif i == 1:
            # If we get the login prompt again, the password was probably
            # refused.
            raise ExceptionPxserial('password refused')
        elif i == 2:
            # If we get the password prompt again,
            # the password was probably refused too.
            raise ExceptionPxserial('password refused')
        elif i == 3:  # Login incorrect -- password was bad.
            raise ExceptionPxserial('permission denied')
        elif i == 4:  # Timeout
            # This is tricky... I presume that we are at the command-line
            # prompt.It may be that the shell prompt was so weird that we
            # couldn't match it. Or it may be that we couldn't log in for
            # some other reason. I can't be sure, but it's safe to guess
            # that we did login because if I presume wrong and we are not
            # logged in then this should be caught later when I try to set
            # the shell prompt.
            pass
        else:
            raise ExceptionPxserial('unexpected login response')

    def logout(self) -> None:
        """Sends exit to the remote shell.

        If there are stopped jobs then this automatically sends exit twice.
        """
        self.sendline("exit")
        index = self.expect([self.login_regex, "(?i)there are stopped jobs"])
        if index == 1:
            self.sendline("exit")
            self.expect(self.login_regex)

    def prompt(self, timeout: int = -1) -> bool:
        """Match the next shell prompt.

        It's little more than a shortcut to :meth:`~fdpexpect.fdspawn.expect`
        method. Note that if you called :meth:`login` with
        `auto_prompt_reset=False`, then before calling :meth:`prompt` you must
        set the :attr:`PROMPT` attribute to a regex that it will use for
        matching the prompt.

        Calling :meth:`prompt` will erase the contents of the :attr:`before`
        attribute even if no prompt is ever matched. If timeout is not given or
        it is set to -1 then self.timeout is used.

        :return: True if the shell prompt was matched, False if the timeout was
                 reached.
        """

        if timeout == -1:
            timeout = self.timeout
        i = self.expect([self.curr_prompt, TIMEOUT], timeout=timeout)
        if i == 1:
            return False
        return True

    def expect(self, pattern, timeout=-1, searchwindowsize=-1,
               async_=False, **kw):
        """Catch the exception from pexpect and replace it with our
        own timeout exception.
        """
        try:
            return super().expect(pattern, timeout, searchwindowsize,
                                  async_, **kw)
        except TIMEOUT:
            raise MestrTimeout

    def set_unique_prompt(self) -> bool:
        """Sets the remote prompt to something more unique than ``#`` or ``$``.
        This makes it easier for the :meth:`prompt` method to match the shell
        prompt unambiguously. This method is called automatically by the
        :meth:`login` method, but you may want to call it manually if you
        somehow reset the shell prompt. For example, if you 'su' to a different
        user then you will need to manually reset the prompt. This sends shell
        commands to the remote host to set the prompt, so this assumes the
        remote host is ready to receive commands.

        Alternatively, you may use your own prompt pattern. In this case you
        should call :meth:`login` with ``auto_prompt_reset=False``; then set
        the :attr:`PROMPT` attribute to a regular expression. After that,
        the :meth:`prompt` method will try to match your prompt pattern.
        """

        self.sendline("unset PROMPT_COMMAND")
        self.sendline(self.prompt_set_sh)  # sh-style
        i = self.expect([TIMEOUT, self.curr_prompt], timeout=10)
        if i == 0:  # csh-style
            self.sendline(self.prompt_set_csh)
            i = self.expect([TIMEOUT, self.curr_prompt], timeout=10)
            if i == 0:
                return False
        return True

    # Those four methods are not documented as part of fdpexpect, and we are
    # encouraged to use directly os.write. However, to preserve
    # the interface compatibility with pxssh, we will reimplement them
    # using serial's write.
    def send(self, s: str) -> int:
        "Write to fd, return number of bytes written"
        s = self._coerce_send_string(s)
        self._log(s, 'send')

        encoded_string = self._encoder.encode(s, final=False)
        return self.serial_object.write(encoded_string)

    def sendline(self, s: str) -> int:
        "Write to fd with trailing newline, return number of bytes written"
        s = self._coerce_send_string(s)
        return self.send(s + self.linesep)

    def write(self, s: str) -> None:
        "Write to fd, return None"
        self.send(s)

    def writelines(self, sequence: List[str]) -> None:
        "Call self.write() for each item in sequence"
        for string in sequence:
            self.write(string)

    def start_logging(self, file_name: str)-> None:
        self.logfile_read: Optional[TimestampedFile] = TimestampedFile(file_name)

    def stop_logging(self)->str:
        self.logfile_read.close()
        file_name = self.logfile_read.name
        self.logfile_read = None
        return file_name

    def clear_input(self)->None:
        """Empty the input buffer, and the internal buffer."""
        self.serial_object.reset_input_buffer()
        self.expect("^.*$")

    def is_state(self, state: str)-> bool:
        """Checks if the serial connection is in a particular state

        :param state: (str) State to check (possible values : "logged in"\
        or "logged out")
        :return: True if the interface is in the right state, false otherwise.
        """
        if state in self._available_states:
            self.sendline("")
            got_prompt = self.prompt(timeout=1)
            return ((state == "logged in" and got_prompt)
                    or (state == "logged out" and not got_prompt))
        return super().is_state(state)
