"""Wrapper module for the pxssh class, to allow it to inherit from Manager"""
from typing import IO, Dict, AnyStr, Optional
import pexpect.pxssh
from pexpect import TIMEOUT
from ..utils.auth_helper import get_creds_from_env
from ..utils.exceptions import MestrTimeout
from ..utils.timestamped_file import TimestampedFile
from .manager import Manager


class SshManager(pexpect.pxssh.pxssh, Manager):  # type: ignore
    """This class is a just a wraper around the pexpect pxssh class.
    It allows us to inherit from Manager and use the login method without
    extra arguments.

    :param auth_creds: base name of env variable where to find credentials. The
            idea is to use credential without pushing it in the git repository
            that we use. The manager will look for ``auth_creds_username`` and
            ``auth_creds_password`` variables to find credentials

    The manager can be defined as :

    .. code-block:: yaml

        -
            type : ssh
            name : yourName
            target : ???
            username : None # optionnal
            password : None # optionnal
            timeout : 30 # optionnal
            maxread : 2000 # optionnal
            searchwindowsize : None # optionnal
            logfile : None # optionnal
            encoding : 'utf-8' # optionnal
            codec_errors : 'strict' # optionnal
            auth_creds : None # optional,
            extra_args_from pxssh...
    """

    # pylint: disable=too-many-arguments
    def __init__(self, target: str,
                 username: Optional[str] = None,
                 password: Optional[str] = None,
                 timeout=30,
                 maxread=2000,
                 searchwindowsize: int = None,
                 logfile: IO = None,
                 encoding='utf-8',
                 codec_errors='strict',
                 auth_creds: str = None,
                 **kwargs):

        super().__init__(timeout=timeout,
                         maxread=maxread,
                         searchwindowsize=searchwindowsize,
                         logfile=logfile, encoding=encoding,
                         codec_errors=codec_errors)
        self.server = target

        if auth_creds:
            self.username, self.password = get_creds_from_env(auth_creds)
        else:
            self.username = username
            self.password = password

        self.opt_login_args: Dict[str, AnyStr] = kwargs
        self._available_states = {"logged in": SshManager.login,
                                  "logged out": SshManager.logout}

    def login(self,
              username: Optional[str] = None,
              password: Optional[str] = None)-> bool:
        "Call login method with arguments stored in the class"
        username = username or self.username
        if not username:
            raise ValueError("Username is not defined")
        password = password or self.password
        if password is None:
            raise ValueError("password is not defined")
        try:
            ret = super().login(server=self.server,
                                username=username,
                                password=password,
                                **self.opt_login_args)
        except pexpect.pxssh.ExceptionPxssh:
            self.pid = None
            raise
        return ret

    @staticmethod
    def man_type()->str:
        """ssh"""
        return "ssh"

    def start_logging(self, file_name: str)->None:
        self.logfile_read: Optional[TimestampedFile] = TimestampedFile(file_name)

    def logout(self)->None:
        """Logs out from the ssh session, then clears the ssh's PID
        to allow us to call login again.
        """
        super().logout()
        self.pid = None

    def expect(self, pattern, timeout=-1, searchwindowsize=-1,
               async_=False, **kw):
        try:
            return super().expect(pattern, timeout, searchwindowsize,
                                  async_, **kw)
        except TIMEOUT:
            raise MestrTimeout

    def stop_logging(self)->str:
        self.logfile_read.close()
        file_name = self.logfile_read.name
        self.logfile_read = None
        return file_name

    def clear_input(self)->None:
        """Empty the internal buffer."""
        self.expect("^.*$")

    def is_state(self, state: str)-> bool:
        """Checks if the ssh connection is in a particular state.

        :param state: (str) State to check (possible values : "logged in"\
        or "logged out")
        :return: True if the interface is in the right state, false otherwise.
        """
        if state == "logged in":
            return hasattr(self, "ptyproc") and self.isalive()
        if state == "logged out":
            return not hasattr(self, "ptyproc") or not self.isalive()

        # raise exception if state is unknown.
        return super().is_state(state)
