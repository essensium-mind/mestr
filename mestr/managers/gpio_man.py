"""manager in charge of managing a GPIO"""
import os
from typing import Union, Callable, Dict, List, Tuple
from .manager import Manager
# adafruit library is not compiling out of beaglebone.
# this try statement is added to remove problem at documentation generation
try:
    from Adafruit_BBIO import GPIO
    from Adafruit_BBIO.GPIO import (RISING, FALLING, BOTH, PUD_OFF, PUD_DOWN
                                    , PUD_UP)
except ImportError:
    from logging import warning
    warning("project won't work")
    RISING = FALLING = BOTH = PUD_OFF = PUD_DOWN = PUD_UP = 0


BASE_PATH = "/sys/class/gpio/"


def dict_gpios()->Dict[str, str]:
    """Returns a dictionnary of 'Label':'gpio' couples
    This function is used to associate the pin name to the
    internal name of the pin to read their value.
    """
    gpios = os.listdir(BASE_PATH)
    list_labels: List[Tuple[str, str]] = []
    for gpio in gpios:
        if gpio.startswith("gpio") and not gpio.startswith("gpiochip"):
            with open(BASE_PATH+gpio+"/label", 'r') as label_file:
                label = label_file.readline().rstrip("\n")
                list_labels.append((label, gpio))
    return dict(list_labels)


class GpioManager(Manager):
    """Class that represent a GPIO port.

    The manager can be defined as :

    .. code-block:: yaml

        -
            type : GPIO
            name : yourName
            gpio : P9_15
            value : 0 # optionnal

    :param gpio: GPIO pin to control
    :param direction: Input (IN/0) or output (OUT/1)
    :param pull_up_down: Internal pull up or down [default=PUD_OFF]
    :param value: initial value of the GPIO [default=0]
    """

    # Direction:
    # - Input
    IN = 0
    # - Output
    OUT = 1

    # Active
    # Active at low logical level, state is High when reading 0
    ACT_LOW = 0
    # Active at high logical level, state is High when reading 1
    ACT_HIGH = 1

    # State :
    # High, logic 1
    HIGH = 1
    # Low, logic 0
    LOW = 0

    # pylint: disable=too-many-arguments
    def __init__(self, gpio: str,
                 direction: Union[int, str] = IN,
                 pull_up_down: Union[int, str] = PUD_OFF,
                 value: int = 0,
                 mode: Union[int, str] = ACT_HIGH):

        self._available_states = {
            "high": lambda self: GpioManager.set_value(self, 1),
            "low": lambda self: GpioManager.set_value(self, 0)}

        if isinstance(direction, str):
            self._direction = getattr(GpioManager, direction)
        else:
            self._direction = direction

        if self._direction == self.IN:
            value = 0
        if isinstance(pull_up_down, str):
            self.pull_up_down = getattr(GPIO, pull_up_down)
        else:
            self.pull_up_down = pull_up_down

        if isinstance(mode, str):
            self.act_high_low = getattr(GpioManager, mode)
        else:
            self.act_high_low = mode

        value = int(bool(value) == bool(self.act_high_low))

        self._pin: str = gpio
        try:
            GPIO.setup(gpio,
                       direction=self._direction,
                       pull_up_down=self.pull_up_down,
                       initial=value)
        except SystemError as excp:
            raise ValueError("GPIO {} doesn't exist".format(gpio)) from excp

    @staticmethod
    def man_type()->str:
        """GPIO"""
        return "GPIO"

    @property
    def pin(self)->str:
        return self._pin

    def is_state(self, state: str)->bool:
        """Checks the state of the GPIO pin.

        :param state: State to check format
        :return: True if the pin is in the right state, false otherwise.
        """
        value = bool(self.get_value())
        value = value if bool(self.act_high_low) else not value
        if state == "high":
            return value
        if state == "low":
            return not value
        return super().is_state(state)

    # actuator :
    def set_value(self, state: int) -> None:
        """Set the state of the GPIO according to the mode

        :param state: 1 means a HIGH value and 0 a LOW value.
        """
        if self._direction == self.OUT:
            if bool(state) == bool(self.act_high_low):
                self.log("Setting GPIO {0} to HIGH\n".format(self._pin))
                GPIO.output(self._pin, self.HIGH)
            else:
                self.log("Setting GPIO {0} to LOW\n".format(self._pin))
                GPIO.output(self._pin, self.LOW)

        else:
            raise RuntimeError("The GPIO is not in output mode.")

    # Sensor :
    def get_value(self)->int:
        """Return the current value of the GPIO pin.

        If in input mode, just reads the pin value.

        If in output mode, we have to check the system file
        storing the value to get it.
        """
        if self._direction == self.OUT:
            dict_labels_gpios = dict_gpios()
            with open("/sys/class/gpio/"+dict_labels_gpios[self._pin]+"/value",
                      "r") as value_file:
                value = int(value_file.readline().rstrip("\n"))
        else:
            value = GPIO.input(self._pin)
        self.log("GPIO {0} has logical value {1}\n".format(self._pin, value))
        return value

    # GPIO specific :
    def set_direction(self, direction: int)->None:
        """Sets the direction of the GPIO

        :param direction: 0/IN or 1/OUT
        """
        if self._direction != direction:
            self._direction = direction
            GPIO.setup(self._pin,
                       direction=self._direction,
                       pull_up_down=self.pull_up_down,
                       initial=self.get_value())

    def wait_for_edge(self, edge: int = BOTH, timeout: float = -1)->None:
        """If the direction is IN, waits for an edge on the GPIO pin.

        :param edge: Edge to wait for. Can be RISING, FALLING or BOTH.
        :param timeout: Time in seconds to wait for an edge (-1: wait forever).
        """
        timeout_ms = int(timeout*1000) if timeout >= 0 else -1
        GPIO.wait_for_edge(self._pin, edge, timeout_ms)
        self.log("Edge detected\n")

    def add_event_detect(self, edge: int = BOTH,
                         callback: Callable = None,
                         bouncetime=0)-> None:
        """Enable edge detection events for the GPIO, if it is configured
            as an input.

        :param edge: Edge to detect. Can be RISING, FALLING or BOTH.
        :param callback: Function to call on event detection.
        :param bouncetime: switch bounce timeout in ms for the callback.
        """

        GPIO.add_event_detect(self._pin, edge, callback, bouncetime)

    def remove_event_detect(self)->None:
        """Remove edge detection for the GPIO, if it is configured
            as an input.
        """
        GPIO.remove_event_detect(self._pin)

    def event_detected(self)->bool:
        """Checks if an edge has been detected on the GPIO.

        The edge detection must be enabled before using this.
        """
        return GPIO.remove_event_detect(self._pin)

    def add_event_callback(self, callback: Callable,
                           bouncetime=0)-> None:
        """Adds a callback function to an already defined event.

        :param callback: Function to call on event detection.
        :param bouncetime: switch bounce timeout in ms for the callback.
        """
        GPIO.add_event_callback(self._pin, callback, bouncetime)
