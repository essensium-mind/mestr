"""Module to give a file-like interface to a logger."""
import logging


class FileLoggerAdapter:
    """Adapts a logger to be used as a file-like object by pexpect

    Makes sure that the lines are not splitted in the log file.
    """

    def __init__(self, logger: logging.Logger, levelno: int = logging.DEBUG):
        self.logger = logger
        self.buffer = ""
        self.levelno = levelno

    def __del__(self)->None:
        if self.buffer:
            self.logger.log(self.levelno, self.buffer)

    def write(self, data: str)->None:
        """Log the given **data**.

        NOTE: data can be a partial line, multiple lines
        but it will be printed line by line.
        """
        lines = data.splitlines(keepends=True)
        if lines:
            lines[0] = self.buffer + lines[0]
            if not lines[len(lines)-1].endswith(('\n', '\r', '\r\n')):
                # If the last line of the data has not ended, keep it for the next time
                self.buffer = lines.pop()
            else:
                self.buffer = ""  # reset the buffer
            for line in lines:
                line = line.rstrip('\r\n')
                if line:  # non-blank
                    self.logger.log(self.levelno, line)

    def flush(self)->None:
        """Does nothing : logging handles the flushing part.
        """
