"""Module to add a timestamp in front of log lines"""
import time


class TimestampedFile():
    """class that adds timestamps in front of log lines.

    This class is made to be used with pexpect, which needs the write
    and the flush methods to be defined. Therefore, only those two methods
    are actually defined.

    :param path: the log file to be written in (must be an open file).

    """

    def __init__(self, path: str):
        self.last_line = "\n"
        self._file = open(path, encoding="utf-8", mode="w")

    def write(self, data: str)-> int:
        r"""Writing method

        Adds the current time in front of the data to print, and write
        it to file, if the precedent data ended with '\n'.
        :param data: the data to log.

        """

        time_stmp = time.time()
        data_to_print = "{0}:\t{1}".format(
            time_stmp, data) if self.last_line.endswith("\n") else data
        self.last_line = data
        return self._file.write(str(data_to_print))

    def write_line(self, data: str)-> int:
        r"""Writing method

        Adds the current time in front of the data to print,
        and write it to file, if the precedent data ended with '\n'.

        :param data: the data to log.

        """
        return self.write(data+"\n")

    @property
    def name(self)->str:
        """Keep compatibility with file object

        :return: location of the file as set on the opening
        """
        return self._file.name

    def close(self)->None:
        """Close the file"""
        self._file.close()

    def flush(self)->None:
        "Flushs the file"
        self._file.flush()
