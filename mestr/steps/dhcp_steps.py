"""Steps linked to network and dhcp related tests"""
import time
import allure
from behave import *
from behave.runner import Context


@When("the User disconnects and reconnects the cable from '{name}'")
@When("the User disconnects and reconnects the cable from '{name}' \
during '{duration:d}'")
def step_impl(context: Context, name: str, duration: int = 20):
    """This step should simulate a network cable short disctonnection.

    .. warning:: For now, it does not work (see documentation -> Area for
        improvements for more informations)

    :param name: The name of the interface which should be disconnected.
    :param duration: Duration of the disconnection, in seconds (default: 20s)
    """
    # For now, we are not able to simulate a cable disconnection.
    raise NotImplementedError("This operation is not yet implemented")
    lan = context.managers[name]
    lan.set_state("down")
    time.sleep(duration)
    lan.set_state("up")


@Then("a DHCP operation should happen on '{name}' within '{timeout:d}' \
seconds")
def step_impl(context: Context, name: str, timeout: int):
    """This step captures dhcp packets on the network, and succeeds
    if in the first transaction captured, a DHCP REQUEST and a DHCP
    ACK are found.

    .. note:: This step is meant as an exemple of what can be done with the
        NetworkManager's dhcp_sniffer. It can be improved for a better analysis
        of the captured packets (make sure that the requested IP matches the one
        stored in the NetworkManager for instance).

    :param name: name of the network manager.
    :param timeout: Duration of the packet capture.
    """
    lan = context.managers[name]
    lan.dhcp_sniffer.timeout = timeout

    print("Beginning sniffing")
    lan.dhcp_sniffer.sniff()
    xid_list = lan.dhcp_sniffer.get_xid_list()
    print("pckts : ", lan.dhcp_sniffer.pckts)
    assert xid_list, "No packets captured"
    msg_types = set()
    transac = lan.dhcp_sniffer.get_dhcp_transac(xid_list[0])
    for i in range(len(transac)):
        msg_types.add(lan.dhcp_sniffer.get_dhcp_msg_type(xid_list[0], i))
    assert 5 in msg_types, "No DHCP ack packet captured"
    assert 3 in msg_types, "No DHCP request packet captured"

